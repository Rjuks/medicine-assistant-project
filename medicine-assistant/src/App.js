import './App.css';
import React, {useState, useEffect} from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import NAVBAR from "./components/navbar/navbar";
import Login from "./components/accountPanel/accountPanel";
import {FOOTER} from "./components/footer/footer";
import {PatientView} from "./components/patientView/patientView";
import axios from "axios";
import AccountPanel from "./components/accountPanel/accountPanel";
import Home from "./components/home/home";
import HowToStart from "./components/howToStart/howToStart";
import {AdminPanel} from "./components/adminPanel/adminPanel";

function App() {
    console.log(sessionStorage.getItem('token eeeeeeeeeeeee'))


  return (
      <BrowserRouter>
        <Switch>
          <React.Fragment>
                  <NAVBAR/>
                  <Route path='/' exact component={Home}/>
                  <Route path='/pacjent'  component={PatientView}/>
                  <Route path='/admin'  component={AdminPanel}/>
                  <Route path='/panel-uzytkownika' exact component={AccountPanel}/>
                  <Route path='/jak-zaczac'  component={HowToStart}/>
                  <FOOTER/>
          </React.Fragment>
        </Switch>
      </BrowserRouter>
  );
}

export default App;
