import React from 'react';
import styles from './modal.module.css';

const Modal = ({closeModal, children, modalTitle})=> {
    return (
        <React.Fragment>
            <div className={styles.backdrop} onClick={closeModal}/>
            <div className={styles.modal}>
                <h2>{modalTitle}</h2>
                <div className={styles.modal__children}>{children}</div>
                <div className={styles.modal__actions}>
                    <button onClick={closeModal}>Wyjdz</button>
                </div>
            </div>
        </React.Fragment>
    );
};

export default Modal;
