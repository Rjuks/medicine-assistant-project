import React, {useState} from 'react'
import styles from './register.module.css'
import axios from "axios";

const RegisterContainer = () => {
    const [inputEmail, setInputEmail] = useState("");
    const [inputPassword, setInputPassword] = useState("");


    return (
        <div className={styles.mainContainer}>
            <h2>
               Zarejestruj się
            </h2>

            <div className={styles.Form__inputsContainer}>
                <input
                    type="text"
                    placeholder='Email'
                    value={inputEmail}
                    onChange={event => setInputEmail(event.target.value)}
                />
                <input
                    type="text"
                    placeholder='Hasło'
                    value={inputPassword}
                    onChange={event => setInputPassword(event.target.value)}
                />
            </div>

            <div className={styles.Form__button}>
                <button onClick={() => {
                    axios.post('/api/Account/Register', {Email: inputEmail, Password: inputPassword})
                        .then(response => {
                            alert(response.data)
                            localStorage.setItem('token', response.data.token)
                        }).catch(e => {
                        alert('Błąd podczas rejestracji')
                    })
                }}>Zarejestruj</button>
            </div>
        </div>
    )
}

export default RegisterContainer
