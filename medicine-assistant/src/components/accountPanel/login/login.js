import React, {useState} from 'react'
import styles from './login.module.css'
import axios from "axios";

const LoginContainer = () => {
    const [inputEmail, setInputEmail] = useState("");
    const [inputPassword, setInputPassword] = useState("");


    return (
        <div className={styles.mainContainer}>
            <h2>
                Zaloguj się
            </h2>

            <div className={styles.Form__inputsContainer}>
                <input
                    type="text"
                    placeholder='Email'
                    value={inputEmail}
                    onChange={event => setInputEmail(event.target.value)}
                />
                <input
                    type="text"
                    placeholder='Hasło'
                    value={inputPassword}
                    onChange={event => setInputPassword(event.target.value)}
                />
            </div>

            <div className={styles.Form__button}>
                <button onClick={() => {
                    axios.post('/api/Account/Login', {Email: inputEmail, Password: inputPassword})
                        .then(response => {
                            if (response.status === 200) {
                                localStorage.setItem('token', response.data.token)
                                alert('Pomyślnie zalogowano.')
                                location.href = '/pacjent'
                            }
                        }).catch(e => {
                        alert('Nieprawidłowy login bądz hasło')
                    })
                }}>Zaloguj</button>
            </div>
</div>
    )
}

export default LoginContainer
