import React, {useState} from 'react'
import styles from './accountPanel.module.css'
import LoginContainer from "./login/login";
import RegisterContainer from "./register/register";

const AccountPanel = () => {
    const [logOrReg, setLogOrReg] = useState(true);

    return (
        <div className={styles.container} >
            <div className={styles.container_buttons}>
                <button onClick={() => setLogOrReg(true)}>Okno logowania</button>
                <button onClick={() => setLogOrReg(false)}>Okno rejestracji</button>
                <button onClick={() => {
                    window.localStorage.clear()
                    alert('Pomyślnie wylogowano')
                }}>Wyloguj</button>
            </div>

            <div className={styles.container_section}>
                {logOrReg ?
                <div className={styles.container_sectionWindow}>
                <LoginContainer />
                </div> :
                <div className={styles.container_sectionWindow}>
                <RegisterContainer />
                </div>}
            </div>
        </div>
    )
}

export default AccountPanel;
