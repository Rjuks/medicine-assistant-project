import React, {useState, useEffect} from 'react';
import styles from "./adminPanel.module.css";
import axios from "axios";
import {ImCross, GoCheck, AiFillEdit} from "react-icons/all";
import {Link} from "react-router-dom";

export const AdminPanel = () => {
    const [allUsers, setAllUsers] = useState([]);
    const [allMedicines, setAllMedicines] = useState([]);

    const [inputAddMedicine, setInputAddMedicine] = useState("");

    const [inputUserLogin, setInputUserLogin] = useState("");
    const [inputUserPassword, setInputUserPassword] = useState("");

    const [inputAdminLogin, setInputAdminLogin] = useState("");
    const [inputAdminPassword, setInputAdminPassword] = useState("");

    const [showEditUser, setShowEditUser] = useState(false);
    const [selectedUserID, setSelectedUserID] = useState();
    const [inputEditUserLogin, setInputEditUserLogin] = useState("");
    const [inputEditUserPassword, setInputEditUserPassword] = useState("");

    const [counterUpdate, setCounterUpdate] = useState(0);


    const [token, setToken] = useState();

    useEffect(() => {
        setToken(localStorage.getItem('token'))

        axios.get('/api/User/Find')
            .then(response => {
                setAllUsers(response.data)
            }).catch(e => {
            if (confirm('Nie masz uprawnień aby korzystać z tej sekcji.')) {
                location.href = '/'
            } else {
                location.href = '/'
            }
        })
    }, [counterUpdate])

    useEffect(() => {
        axios.get('/api/Medicine/Find')
            .then(response => {
                setAllMedicines(response.data)
            })
        }, [counterUpdate])


    console.log(allMedicines, 'allmed')
    console.log(allUsers, 'all users')
    return (
      <main className={styles.mainContainer}>
        <section>
            <div className={styles.add_content}>
                <h2>Dodaj użytkownika do bazy</h2>
                <div className={styles.add_content_inputs}>
                    <input
                        type="text"
                        placeholder='Wpisz login..'
                        value={inputUserLogin}
                        onChange={event => setInputUserLogin(event.target.value)}
                    />
                    <input
                        type="text"
                        placeholder='Wpisz hasło..'
                        value={inputUserPassword}
                        onChange={event => setInputUserPassword(event.target.value)}
                    />
                    <button onClick={() => {
                        axios.post('api/User/CreateUser ', {Email: inputUserLogin, Password: inputUserPassword}, {headers: {Authorization: `Bearer ${token}`}})
                            .then(response => {
                                alert('Pomyślnie dodano użytkownika.')
                                setCounterUpdate(counterUpdate+1);
                            }).catch(e => {
                            alert('Błąd podczas dodawania użytkownika.')
                        })
                    }}>
                        Dodaj użytkownika
                    </button>
                </div>
            </div>

            <div>
                <h2>Wszyscy użytkownicy</h2>
                <ul className={styles.list}>
                    {allUsers.map((singleUser, index) =>
                        <>
                        <li key={index} className={styles.list_item}>{singleUser.email}
                            <div>
                                <span
                                    onClick={() => {
                                        setShowEditUser(!showEditUser)
                                        setSelectedUserID(singleUser.id)
                                    }
                                    }>
                                    <AiFillEdit/>
                                </span>
                                <span
                                    onClick={() => {
                                    axios.delete(`api/User/Delete/${singleUser.id}`, {headers: {Authorization: `Bearer ${token}`}})
                                        .then(response => {
                                            alert('Pomyślnie usunięto użytkownika z bazy.')
                                            setCounterUpdate(counterUpdate+1);
                                        }).catch(e => {
                                        alert('Błąd podczas usuwania użytkownika z bazy.')
                                    })
                                }}><ImCross/>
                                </span>
                            </div>
                        </li>
                            {showEditUser && selectedUserID === singleUser.id &&
                                <div className={styles.list_item_edit}>
                                    <input
                                        type="text"
                                        placeholder='Wpisz login..'
                                        value={inputEditUserLogin}
                                        onChange={event => setInputEditUserLogin(event.target.value)}
                                    />
                                        <input
                                        type="text"
                                        placeholder='Wpisz hasło..'
                                        value={inputEditUserPassword}
                                        onChange={event => setInputEditUserPassword(event.target.value)}
                                        />
                                        <span onClick={() => {
                                            axios.put(`api/User`, {Email: inputEditUserLogin, Password: inputEditUserPassword, Id: singleUser.id} ,{headers: {Authorization: `Bearer ${token}`}})
                                                .then(response => {
                                                    alert('Pomyślnie zaktualizowano użytkownika.')
                                                    setCounterUpdate(counterUpdate+1);
                                                }).catch(e => {
                                                alert('Błąd podczas zaktualizowania użytkownika.')
                                            })
                                        }}><GoCheck/></span>
                                </div>
                            }
                        </>
                    )}
                </ul>
            </div>
        </section>

          <section>
              <div className={styles.add_content}>
                  <h2>Dodaj admina do bazy</h2>
                  <div className={styles.add_content_inputs}>
                      <input
                          type="text"
                          placeholder='Wpisz login..'
                          value={inputAdminLogin}
                          onChange={event => setInputAdminLogin(event.target.value)}
                      />
                      <input
                          type="text"
                          placeholder='Wpisz hasło..'
                          value={inputAdminPassword}
                          onChange={event => setInputAdminPassword(event.target.value)}
                      />
                      <button onClick={() => {
                          axios.post('api/User/CreateAdmin ', {Email: inputAdminLogin, Password: inputAdminPassword}, {headers: {Authorization: `Bearer ${token}`}})
                              .then(response => {
                                  alert('Pomyślnie dodano admina.')
                                  setCounterUpdate(counterUpdate+1);
                              }).catch(e => {
                              alert('Błąd podczas dodawania admina.')
                          })
                      }}>
                          Dodaj admina
                      </button>
                  </div>
              </div>

          </section>

        <section>
            <div className={styles.add_content}>
                <h2>Dodaj lek do bazy</h2>
                <div className={styles.add_content_inputs}>
                <input
                    type="text"
                    placeholder='Wpisz nazwę leku..'
                    value={inputAddMedicine}
                    onChange={event => setInputAddMedicine(event.target.value)}
                />
                <button onClick={() => {
                    axios.post('api/Medicine/Create ', {Name: inputAddMedicine}, {headers: {Authorization: `Bearer ${token}`}})
                        .then(response => {
                            alert('Pomyślnie dodano lek.')
                            setCounterUpdate(counterUpdate+1);
                        }).catch(e => {
                        alert('Błąd podczas dodawania leku do bazy.')
                    })
                }}>
                    Dodaj lek
                </button>
                </div>
            </div>

            <div>
                <h2>Wszystkie leki</h2>
                <ul className={styles.list}>
                    {allMedicines.map((singleMed, index) =>
                        <>
                            <li key={index} className={styles.list_item}>{singleMed.name}
                                <span
                                    onClick={() => {
                                        axios.delete(`api/Medicine/Delete/${singleMed.id}`, {headers: {Authorization: `Bearer ${token}`}})
                                            .then(response => {
                                                alert('Pomyślnie usunięto lek.')
                                                setCounterUpdate(counterUpdate+1);
                                            }).catch(e => {
                                            alert('Błąd podczas usuwania leku z bazy.')
                                        })
                                    }}><ImCross/>
                            </span>
                            </li>
                        </>
                    )}
                </ul>
            </div>
        </section>
    </main>
  )
}
