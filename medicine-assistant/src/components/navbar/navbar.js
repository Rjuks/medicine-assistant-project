import React from 'react'
import styles from './navbar.module.css'
import { Link } from 'react-router-dom'
import Baner from '../../images/baner.png'
import Logo from '../../images/logo.png'

//w fetch dac te api co daje baza localhost21321/api  i niby to juz bedzie cos

const NAVBAR = () => {
    return (
        <header>
        <div className={styles.navbar}>
            <Link to={'/'}>
                <div className={styles.navbar__logo}>
                    <img className={styles.logo__image} src={Logo} alt='logo'/>
                    <span className={styles.logo__title}>Asystent przyjmowania leków</span>
                </div>
            </Link>
            <div className={styles.navbar__links}>
                <Link to={'/'} style={{ textDecoration: 'none', color: 'black' }}>
                    <div className={styles.link_text}>
                        <span>Strona główna</span>
                    </div>
                </Link>

                <Link to={'/jak-zaczac'} style={{ textDecoration: 'none', color: 'black' }}>
                    <div className={styles.link_text}>
                        <span>Jak zacząć</span>
                    </div>
                </Link>

                <Link to={'/pacjent'} style={{ textDecoration: 'none', color: 'black' }}>
                    <div className={styles.link_text}>
                        <span>Pacjent</span>
                    </div>
                </Link>

                <Link to={'/panel-uzytkownika'} style={{ textDecoration: 'none', color: 'black' }}>
                    <div className={styles.link_text}>
                        <span>Panel startowy</span>
                    </div>
                </Link>

                <Link to={'/admin'} style={{ textDecoration: 'none', color: 'black' }}>
                    <div className={styles.link_text}>
                        <span>Admin</span>
                    </div>
                </Link>
            </div>
        </div>
            <div>
                <img className={styles.banner} src={Baner} alt='banner' />
            </div>
        </header>
    )
}

export default NAVBAR
