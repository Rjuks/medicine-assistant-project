import React from 'react'
import {FiArrowDownCircle} from "react-icons/all";
import styles from './howToStart.module.css'

const HowToStart = () => (
    <main className={styles.MainContainer}>

        <h1 className={styles.Container_Header}>Tutaj dowiesz jak korzystać z asystenta przyjmowania leków :)</h1>

        <div className={styles.HowToStartSection}>
            <h2>
                ZAŁÓŻ KONTO
            </h2>
            <p>
                Zarejestruj się w aplikacji wpisując swoje dane i zaloguj się.
            </p>

            <div className={styles.Section_Icon}>
                <FiArrowDownCircle/>
            </div>
            <div className={styles.Section_CenterHeader}>
                <h2>
                    PLUSY KORZYSTANIA Z NASZEJ APLIKACJI
                </h2>
            </div>

            <div className={styles.Section_PlanDescription}>
                <div className={styles.PlanDescription_first}>
                    <h2>
                        ULATW SOBIE ŻYCIE
                    </h2>
                    <p>
                        Korzystając z naszej aplikacji nie musisz myśleć i zadręczać sobie głowy. Tworzysz plan przyjmowania leków i gotowe!
                    </p>
                </div>
                <div>
                    <h2>
                        ŁATWE W OBSŁUDZE
                    </h2>
                    <p>
                        Nasza aplikacja została stworzona z myslą o prostocie i funcjonalności - łatwa i przyjemna w użyciu
                    </p>
                </div>
            </div>

            <div className={styles.Section_Icon}>
                <FiArrowDownCircle/>
            </div>


            <h2>I GOTOWE </h2>
            <p>Po stworzeniu konta możesz cieszyć się Łatwiejszym życiem. Miłego dnia!</p>


            <div className={styles.Section_Icon}>
                <FiArrowDownCircle/>
            </div>

            <div className={styles.Section_Button}>
                <a href='/panel-uzytkownika'>
                    Przejdż do panelu
                </a>
            </div>

        </div>
    </main>
)

export default HowToStart;
