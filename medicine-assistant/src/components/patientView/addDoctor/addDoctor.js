import React, {useState} from 'react'
import styles from './addDoctor.module.css'
import axios from "axios";

const AddDoctor = ({setUpdate, update}) => {
    const [inputDoctorName, setInputDoctorName] = useState('');
    const [inputDoctorSurname, setInputDoctorSurname] = useState('')
    const [inputDoctorPhone, setInputDoctorPhone] = useState('')

    const [token, setToken] = useState();


    return (
        <div className={styles.Form__inputsContainer}>
            <p>Podaj imię</p>
            <input
                type="text"
                placeholder='Imię..'
                value={inputDoctorName}
                onChange={event => setInputDoctorName(event.target.value)}
            />
            <p>Podaj nazwisko</p>
            <input
                type="text"
                placeholder='Nazwisko..'
                value={inputDoctorSurname}
                onChange={event => setInputDoctorSurname(event.target.value)}
            />
            <p>Podaj numer telefonu</p>
            <input
                type="text"
                placeholder='Numer telefonu..'
                value={inputDoctorPhone}
                onChange={event => setInputDoctorPhone(event.target.value)}
            />

            <div className={styles.Form__button}>
                <input type="submit" value="Dodaj" onClick={() => {
                    axios.post(`/api/Doctor`, {Name: inputDoctorName, Surname: inputDoctorSurname, PhoneNumber: inputDoctorPhone},
                        {headers: {Authorization: `Bearer ${token}`}})
                        .then(response => {
                            alert('Pomyślnie dodano doktora.')
                            setUpdate(update+1);
                        } )
                        .catch(e=> alert('Dodawanie doktora nie udało sie.'))
                }} />
            </div>
        </div>
    )
}

export default AddDoctor
