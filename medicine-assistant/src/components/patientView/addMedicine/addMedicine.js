import React, {useEffect, useState} from 'react'
import Modal from "../../UI/Modal/modal";
import styles from './addMedicine.module.css'
import axios from "axios";
import {frequencyOptions, doseOptions} from "./additionalData";

const AddMedicine = ({update, setUpdate}) => {
    const [allMedicines, setAllMedicines] = useState([])

    const [chooseMed, setChooseMed] = useState(0);
    const [chooseDose, setChooseDose] = useState("Nie podano")
    const [chooseFreq, setChooseFreq] = useState("Nie podano")
    const [useDate, setUseDate] = useState(new Date());
    const [token, setToken] = useState();

    useEffect(() => {
        setToken(localStorage.getItem('token'))

        axios.get('/api/Medicine/Find')
            .then(response => {
                setAllMedicines(response.data)
            })
    }, [])

    console.log(allMedicines, 'Wszystkie leki')
    console.log(chooseMed, 'Wybrany lek')

    return (
        <div className={styles.Form__inputsContainer}>

            <p>Wybierz lek</p>
            <select value={chooseMed} onChange={event => setChooseMed(event.target.value)}>
                {allMedicines.map((singleMed, index) => (
                    <option key={index} value={singleMed.id} onChange={() => {}}>{singleMed.name}</option>
                ))}
            </select>

            <p>Wybierz dawke</p>
            <select value={chooseDose} onChange={event => setChooseDose(event.target.value)}>
                {doseOptions.map((option, index) => (
                    <option key={index} value={option.value}>{option.label}</option>
                ))}
            </select>
            <p>Wybierz czestotliwość</p>
            <select value={chooseFreq} onChange={event => setChooseFreq(event.target.value)}>
                {frequencyOptions.map((option, index) => (
                    <option key={index} value={option.value}>{option.label}</option>
                ))}
            </select>
            <p>Do</p>
            <input
                type="date"
                value={useDate}
                onChange={event => setUseDate(event.target.value)}
            />

            <div className={styles.Form__button}>
                <input type="submit" value="Dodaj" onClick={() => {
                    axios.post(`/api/Medicine/Add`, {MedicineId: parseInt(chooseMed), UseDate: useDate, Dose: chooseDose, Frequency: chooseFreq},
                        {headers: {Authorization: `Bearer ${token}`}})
                        .then(response => {
                            alert('Pomyślnie dodano plan.')
                            setUpdate(update+1);
                        })
                        .catch(e=> alert('Plan z tym lekiem jest juz użyty.'))
                }} />
            </div>
        </div>
    )
}

export default AddMedicine
