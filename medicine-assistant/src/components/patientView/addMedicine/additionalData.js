export const frequencyOptions = [
    {
        label: "Raz dziennie",
        value: "Raz dziennie",
    },
    {
        label: "Dwa razy dziennie",
        value: "Dwa razy dziennie",
    },
    {
        label: "Trzy razy dziennie",
        value: "Trzy razy dziennie",
    },
    {
        label: "Co 12h",
        value: "Co 12h",
    },
    {
        label: "Co 24h",
        value: "Co 24h",
    },
    {
        label: "Co drugi dzień",
        value: "Co drugi dzień",
    },
    {
        label: "Co tydzień",
        value: "Co tydzień",
    },
];


export const doseOptions = [
    {
        label: "1/2 tabletki",
        value: "1/2 tabletki",
    },
    {
        label: "1 tabletka",
        value: "1 tabletka",
    },
    {
        label: "2 tabletki",
        value: "2 tabletki",
    },
    {
        label: "3 tabletki",
        value: "3 tabletki",
    },
    {
        label: "1mg",
        value: "1mg",
    },
    {
        label: "2mg",
        value: "2mg",
    },
    {
        label: "3mg",
        value: "3mg",
    },
    {
        label: "1ml",
        value: "1ml",
    },
    {
        label: "2ml",
        value: "2ml",
    },
    {
        label: "3ml",
        value: "3ml",
    },
    {
        label: "1 miarka",
        value: "1 miarka",
    },
    {
        label: "2 miarki",
        value: "2 miarki",
    },
    {
        label: "3 miarki",
        value: "3 miarki",
    },
];
