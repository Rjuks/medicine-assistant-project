import React, {useState, useEffect} from 'react';
import styles from './patientView.module.css'
import AddMedicine from "./addMedicine/addMedicine";
import axios from "axios";
import AddDoctor from "./addDoctor/addDoctor";
import {HiCursorClick, BsFillPersonFill, AiFillPhone, ImCross} from "react-icons/all";
import moment from 'moment'

export const PatientView = () => {
const [openAddPlan, setOpenAddPlan] = useState(false)
const [openAddDoctor, setOpenAddDoctor] = useState(false)
const [token, setToken] = useState(localStorage.getItem('token'));
const [allDoctors, setAllDoctors] = useState([])
const [getUserMedicines, setGetUserMedicines] = useState([])
const [counterUpdate, setCounterUpdate] = useState(0);

    useEffect(() => {
        setToken(localStorage.getItem('token'))
        if(allDoctors) (
        axios.get('/api/Doctor/Find')
            .then(response => {
                setAllDoctors(response.data)
            })
        )
    }, [counterUpdate])


    useEffect(() => {
        axios.get('/api/Medicine/GetUserMedicines')
            .then(response => {
                setGetUserMedicines(response.data)
            }).catch(e => {
                alert('Musisz się zalogować!')
            location.href = '/panel-uzytkownika';
        })
    }, [counterUpdate])


    return (
    <main className={styles.mainContainer}>
        <section className={styles.section_left}>
            <div className={styles.section_singleColumn}>
                <h2>Nazwa</h2>
                <h2>Dawka</h2>
                <h2>Częstotliwość</h2>
                <h2>Data dodania</h2>
                <h2>Do kiedy</h2>
            </div>
                {getUserMedicines.map((item, index) => {
                        return (
                            <div key={index} className={styles.section_singleColumn}>
                                    <p className={styles.singleColumn_name}>{item.medicine.name}</p>
                                    <p className={styles.singleColumn_dose}>{item.dose}</p>
                                    <p className={styles.singleColumn_dose}>{item.frequency}</p>
                                    <p className={styles.singleColumn_date}>{moment(item.entryDate).format('MM/DD/YYYY')}</p>
                                    <p className={styles.singleColumn_dose}>{moment(item.useDate).format('MM/DD/YYYY')}</p>
                                    <span className={styles.singleColumn_deleteIcon} onClick={() => {
                                        axios.delete(`api/Medicine/DeleteFromUser/${item.medicine.id}`, {headers: {Authorization: `Bearer ${token}`}})
                                            .then(response => {
                                                alert('Pomyślnie usunięto plan.')
                                                setCounterUpdate(counterUpdate+1);
                                            }).catch(e => {
                                            alert('Błąd podczas usuwania planu z bazy.')
                                        })
                                    }}><ImCross/></span>
                            </div>
                        )
                })}

        </section>

        <section className={styles.section_right}>
            <div className={styles.section_right_doctors}>
                <h2 className={styles.section_right_doctors_header}>Lista doktorów</h2>
                <ul>
                    {allDoctors.map((singleDoctor, index) => {
                        return <li key={index}>
                            <span className={styles.singleDoctor_name}><BsFillPersonFill/>{singleDoctor.name} {singleDoctor.surname}</span>
                            <span className={styles.singleDoctor_phone}><AiFillPhone/>{singleDoctor.phoneNumber}</span>
                            <span className={styles.singleDoctor_deleteIcon} onClick={() => {
                                axios.delete(`api/Doctor/Delete/${singleDoctor.id}`, {headers: {Authorization: `Bearer ${token}`}})
                                    .then(response => {
                                        alert('Pomyślnie usunięto doktora.')
                                        setCounterUpdate(counterUpdate+1);
                                    }).catch(e => {
                                    alert('Błąd podczas usuwania doktora z bazy.')
                                })
                            }}><ImCross/></span>
                        </li>
                    })}
                </ul>
            </div>
            <div className={styles.addSection}>
                <div className={styles.addSection_header} onClick={() => setOpenAddPlan(!openAddPlan)}>
                    <h2>Kliknij aby dodać plan</h2>
                    <span><HiCursorClick /></span>
                </div>
                {openAddPlan && <AddMedicine setUpdate={setCounterUpdate} update={counterUpdate}/>}
            </div>

            <div className={styles.addSection}>
                <div className={styles.addSection_header} onClick={() => setOpenAddDoctor(!openAddDoctor)}>
                    <h2>Kliknij aby dodać doktora </h2>
                    <span><HiCursorClick /></span>
                </div>
                {openAddDoctor && <AddDoctor setUpdate={setCounterUpdate} update={counterUpdate} />}
            </div>
        </section>
    </main>
    )
}


