import React from 'react'
import styles from './footer.module.css'

export const FOOTER =  () => (
    <footer className={styles.container}>
        <div className={styles.container_footer}>
            <p>Copyright © PWSiP. Wszelkie prawa zastrzeżone</p>
        </div>
    </footer>
)
