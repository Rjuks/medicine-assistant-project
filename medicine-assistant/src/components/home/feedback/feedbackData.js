export const feedbacksData = [
    {
        personName: 'Karol O.',
        text: 'Aplikacja fest ułatwiła mi życie!. '
    },
    {
        personName: 'Marcin A.',
        text: 'Super aplikacja! Moje życie zmieniło się o 180 stopni.'
    },
    {
        personName: 'Bartosz Sz.',
        text: 'Fajna aplikacja, mogę sobie zapisać wszystkie leki i nie myśleć na zapas.'
    },
]
