import React from "react";
import {FeedbackCard} from "../feedbackCard/feedbackCard";


export const FeedbackCardsGenerator = (props) => {
    console.log()
    return props.cardsData.map((singleCard, index) => (
        <FeedbackCard key={index} cardData={singleCard} />
    ))
}

