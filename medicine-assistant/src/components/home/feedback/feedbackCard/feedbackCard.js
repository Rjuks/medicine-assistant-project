import React from 'react';
import styles from './feedbackCard.module.css';
import {GoQuote} from "react-icons/all";


export const FeedbackCard = (props) => (
    <div className={styles.Feedback_singleCard}>
        <div>
            <GoQuote />
        </div>
        <p className={styles.singleCard_text}>{props.cardData.text}</p>

        <div className={styles.singleCard_personContainer}>
           <p className={styles.singleCard_person}>{props.cardData.personName}</p>
        </div>
    </div>
)
