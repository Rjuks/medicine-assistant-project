import React from 'react'
import styles from './home.module.css'
import {FaArrowDown} from "react-icons/all";
import {feedbacksData} from "./feedback/feedbackData";
import {FeedbackCardsGenerator} from "./feedback/feedbackCardsGenerator/feedbackCardsGenerator";

const Home = () => (
    <main className={styles.MainContainer}>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320" style={{position: 'absolute', left: 0, top: '-5px', zIndex: -1}}>
            <path fill="#37aefa" fillOpacity="1" d="M0,256L48,261.3C96,267,192,277,288,261.3C384,245,480,203,576,192C672,181,768,203,864,186.7C960,171,1056,117,1152,112C1248,107,1344,149,1392,170.7L1440,192L1440,0L1392,0C1344,0,1248,0,1152,0C1056,0,960,0,864,0C768,0,672,0,576,0C480,0,384,0,288,0C192,0,96,0,48,0L0,0Z"></path>
        </svg>

            <h1 className={styles.Container_Header}>Witaj na stronie głównej</h1>

        <section className={styles.Container_Content}>

            <h2 className={styles.Content_Header}>Aplikacja została stworzona z myślą o ludziach którzy lubią zaplanować i ułatwic sobie życie :)</h2>

            <div className={styles.Content_Description}>
                <p>Za pomocą aplikacji stworzysz własne harmonogramy przyjmowania leków z dokładną dawka, datą i informacją czy nie przeoczyłes dnia!</p>
                <p>Mnóstwo użytkowników jest zadowolona z działania i korzyści naszej aplikacji, sam zobacz opinie!</p>
            </div>
            <div className={styles.Content_Icon}>
                <FaArrowDown/>
            </div>


            <div className={styles.Content_cards}>
                <FeedbackCardsGenerator cardsData={feedbacksData} />
            </div>

        </section>
    </main>
)

export default Home;
