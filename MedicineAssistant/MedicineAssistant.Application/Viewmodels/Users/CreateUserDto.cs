﻿namespace MedicineAssistant.Application.Viewmodels.Users
{
	public class CreateUserDto
	{
		public string Email { get; set; }
		public string Password { get; set; }
	}
}