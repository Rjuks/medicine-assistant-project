﻿namespace MedicineAssistant.Application.Viewmodels.Users
{
	public class UpdateUserDto
	{
		public string Id { get; set; }
		public string Email { get; set; }
		public string Password { get; set; }
	}
}