﻿namespace MedicineAssistant.Application.Viewmodels.Users
{
	public class GetUserDto
	{
		public string Email { get; set; }
		public string Password { get; set; }
		public string Id { get; set; }
	}
}